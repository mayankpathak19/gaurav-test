def detecter(element):
    def isIn(sequence):
        return element in sequence
    return isIn


detect30, detect45 = detecter(30), detecter(45)
