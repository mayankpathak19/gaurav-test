# Add the factory function implementation here
def factory(n=0):
    def current():
        return n
    def counter():
        return n + 1
    return current, counter
